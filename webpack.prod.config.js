const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
  mode: "production",
  plugins: [new HtmlWebpackPlugin({
    template: 'app/public/index.html',
    favicon: 'app/public/favicon.ico',
    inject: false
  })],
});
