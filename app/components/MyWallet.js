import React from "react";
import Balance from "./Balance";
import PropTypes from 'prop-types';


class StockRecord extends React.Component {
    constructor() {
        super();
        this.state = {
            sellAmount: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.sellStock = this.sellStock.bind(this);
    }

    sellStock(assetIndex) {
        this.props.sellStock(assetIndex, this.state.sellAmount);
        this.setState({
            sellAmount: ""
        });
    }
    handleChange(e) {
        this.setState({
            sellAmount: e.target.value
        });
    }
    render() {
        const props = this.props;
        return (
            <tr key={props.index}>
                <th>{props.companyName}</th>
                <th>{props.stockAmount}</th>
                <th>
                    <input
                        type="number"
                        placeholder="Insert amount..."
                        onChange={this.handleChange}
                        value={this.state.sellAmount}
                    />
                    <button onClick={() => this.sellStock(props.index)}>Sell</button>
                </th>
            </tr>
        )
    }
}

const MyAssets = (props) => {
    const assetArray = props.assets;
    return (
        <table className="assets-table">
            <tbody>
                <tr>
                    <th>Company</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
                {assetArray.map((asset, index) => {
                    return (
                        <StockRecord
                            key={index}
                            index={index}
                            companyName={asset.name}
                            stockAmount={asset.amount}
                            sellStock={props.sellStock}
                        />
                    )
                })}
            </tbody>
        </table>
    )
}

export default class MyWallet extends React.Component {

    constructor() {
        super();
        this.state = {
            sellAmount: 0,
        }
    }

    render() {
        return (
            <div className="my-wallet">
                <Balance
                    balance={this.props.balance}
                    addToBalance={this.props.addToBalance}
                    removeFromBalance={this.props.removeFromBalance}
                />
                <p>My current assets:</p>
                <MyAssets
                    assets={this.props.assets}
                    sellStock={this.props.sellStock} />
            </div>
        );
    }
}

MyAssets.propTypes = {
    assets: PropTypes.array.isRequired
}

MyWallet.propTypes = {
    assets: PropTypes.array.isRequired,
    balance: PropTypes.number.isRequired,
    addToBalance: PropTypes.func.isRequired,
    removeFromBalance: PropTypes.func.isRequired
}