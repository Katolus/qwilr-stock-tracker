import React from "react";
import PropTypes from 'prop-types';
import { auth, api_url } from "../config";

class Search extends React.Component {

    constructor() {
        super();
        this.state = {
            searchText: "",
            queryData: [],
            searchedAssets: []
        }

        this.fetchIndex = this.fetchIndex.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({
            searchText: e.target.value
        })

    }


    fetchIndex() {
        const fetchString = api_url + "companies?query=" + this.state.searchText;
        let queryResult;
        fetch(fetchString, {
            method: "GET",
            headers: {
                'Authorization': auth
            }
        }).then(response => response.json())
            .then(response => queryResult = response.data)
            .then(() => queryResult ? this.props.addToAssets(queryResult) : null)
            .catch(err => console.log('Unable to fetch the codes. Err -> ', err));


        this.setState({
            searchedAssets: [...this.state.searchedAssets, this.state.searchText],
            searchText: ""
        });
    }

    render() {
        return (
            <div className="search-bar">
                <input className="input-bar"
                    onChange={this.handleChange}
                    value={this.state.searchText}
                    type="text"
                    placeholder="Type your researched stock asset..."
                />
                <button onClick={this.fetchIndex}>Search</button>
            </div>
        )
    }
}

Search.propTypes = {
    addToAssets: PropTypes.func.isRequired
}

export default Search;