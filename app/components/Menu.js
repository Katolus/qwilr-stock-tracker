import React from "react";
import PropTypes from 'prop-types';



const MenuButton = (props) => {
    const { action, name, url } = props;
    return (
        <div className="menu-button">
            {action ? (
                <a onClick={() => action()} href={url ? url : "/"}>{name}</a>
            ) : <a href={url ? url : "/"}>{name}</a>}
        </div>
    )
}

class Menu extends React.Component {

    constructor() {
        super();
        this.refreshData = this.refreshData.bind(this);
    }

    refreshData(stateItem) {
        if (stateItem) {
            localStorage.removeItem(stateItem);
        } else {
            localStorage.clear();
        }
    }

    render() {
        return (
            <div className="menu">
                <MenuButton name="Home" />
                <MenuButton name="My wallet" url="/wallet" />
                <MenuButton name="Refresh local storage" action={this.refreshData} />
            </div>
        )
    }

}

MenuButton.propTypes = {
    action: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
}
export default Menu;