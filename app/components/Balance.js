import React from "react";
import PropTypes from 'prop-types';
import { Balance as BalanceTitle } from "./Home";
export default class Balance extends React.Component {

    constructor() {
        super();
        this.state = {
            input: 0
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({
            input: e.target.value
        });
    }

    render() {
        const { balance, addToBalance, removeFromBalance } = this.props;
        return (
            <div className="balance">
                <BalanceTitle balance={balance}/>
                <div className="balance-input">
                    <input type="number" placeholder="Insert amount..." onChange={this.handleChange} />
                    <button onClick={() => addToBalance(this.state.input)}>Add</button>
                    <button onClick={() => removeFromBalance(this.state.input)}>Subtract</button>
                </div>
            </div>
        )
    }
}

Balance.propTypes = {
    balance: PropTypes.number.isRequired,
    addToBalance: PropTypes.func,
    removeFromBalance: PropTypes.func
}