import React from "react";
import PropTypes from 'prop-types';
import { auth, api_url } from "../config";

const getWeekBeforeTodayDate = (howManyDaysAgo) => {
    const todayArray = new Date().toISOString().slice(0, 10).split("-");
    let currentYear = parseInt(todayArray[0]);
    let currentMonth = parseInt(todayArray[1]);
    let currentDay = parseInt(todayArray[2]);

    return new Date(currentYear, currentMonth - 1, currentDay - howManyDaysAgo + 1).toISOString().slice(0, 10);
}

class StockAsset extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            operationMessage: "",
            loading: false,
            amount: "",
            price: "Unknown",
            assetQuery: []
        }

        this.fetchIndex = this.fetchIndex.bind(this);
        this.buyStock = this.buyStock.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.updateOperationStatus(false);
        this.setState({
            amount: e.target.value,
        });
    }

    buyStock() {
        this.props.updateOperationStatus(true);
        const cost = parseFloat(this.state.amount * this.state.price).toFixed(2);
        if (this.props.balance - cost < 0) {
            this.props.updateOperationMessage("Not enough money!");
        }
        else if (this.state.amount <= 0) {
            this.props.updateOperationMessage("Invalid amount!");
        } else {
            const operationMessage = `Successfully bought ${this.props.name} for ${cost}$`;
            this.props.buyStock({
                amount: this.state.amount,
                code: this.props.code,
                cost: cost,
                name: this.props.name,
                price: this.state.price
            });
            this.props.updateOperationMessage(operationMessage);
        }
        this.setState({
            amount: ""
        });
    }

    fetchIndex() {
        this.setState({
            loading: true
        });

        const fetchString = api_url + "prices?identifier=" + this.props.code + "&start_date=" + getWeekBeforeTodayDate(7);
        let queryResult;

        fetch(fetchString, {
            method: "GET",
            headers: {
                'Authorization': auth
            }
        }).then(response => response.json())
            .then(response => queryResult = response.data)
            .then(() => {
                this.setState({
                    loading: false,
                    price: queryResult[0].close,
                    assetQuery: queryResult
                })
            })
            .catch(err => {
                this.props.updateOperationMessage("Unable to fetch price for " + this.props.name);
                this.props.updateOperationStatus(true);
                console.log('Unable to fetch the this resource. Err -> ', err);
            })
            .finally(() => this.setState({
                loading: false
            }));
    }

    render() {

        return (
            <div className="asset-row">
                <span>{this.props.code}</span>
                <span>{this.props.name}</span>
                <span>Price:
                {this.state.loading ? (
                        "loading..."
                    ) : (
                            ` ${this.state.price} $`
                        )}
                </span>
                <button onClick={() => this.fetchIndex()}>Fetch price</button>
                {this.state.price !== "Unknown" ? (
                    <span>
                        <input
                            type="number"
                            placeholder="Insert amount..."
                            onChange={this.handleChange}
                            value={this.state.amount}
                        />
                        <button onClick={() => this.buyStock()}>Buy</button>
                    </span>
                ) : null}
            </div>
        )
    }
}


StockAsset.propTypes = {
    balance: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    code: PropTypes.string.isRequired,
    updateOperationMessage: PropTypes.func.isRequired,
    updateOperationStatus: PropTypes.func.isRequired,
    buyStock: PropTypes.func.isRequired,
}

export { StockAsset, getWeekBeforeTodayDate };