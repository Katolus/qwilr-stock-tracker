import React from 'react';

export const Whoops = () => (
    <div >
        Requested site not found!
  </div>
);