import React from 'react';
import Menu from "./Menu";
import MyWallet from "./MyWallet";
import Search from "./Search";
import { getWeekBeforeTodayDate } from "./StockAsset";
import { Home } from "./Home";
import { auth, api_url } from "../config";
import '../css/main-styles.scss';
import PropTypes from 'prop-types';

export default class App extends React.Component {

    constructor() {
        super();

        this.state = {
            isMyWallet: false,
            balance: 10000.00,
            myStock: [],
            latestQuery: [],
            assets: []
        }

        this.addToBalance = this.addToBalance.bind(this);
        this.removeFromBalance = this.removeFromBalance.bind(this);
        this.addToAssets = this.addToAssets.bind(this);
        this.buyStock = this.buyStock.bind(this);
        this.sellStock = this.sellStock.bind(this);
    }

    componentDidMount() {
        const cachedBalance = parseFloat(localStorage.getItem("balance"));
        const cachedMyStock = JSON.parse(localStorage.getItem("myStock"));
        const cachedLatestQuery = JSON.parse(localStorage.getItem("latestQuery"));
        const cachedAssets = JSON.parse(localStorage.getItem("assets"));

        this.setState({
            isMyWallet: this.props.location.pathname === '/wallet',
            balance: cachedBalance ? cachedBalance : 10000,
            myStock: cachedMyStock ? cachedMyStock : [],
            latestQuery: cachedLatestQuery ? cachedLatestQuery : [],
            assets: cachedAssets ? cachedAssets : []
        })
    }

    componentDidUpdate() {
        for (let stateProperty in this.state) {
            const cachedValue = localStorage.getItem(stateProperty);
            cachedValue !== this.state[stateProperty] ? localStorage.setItem(stateProperty, JSON.stringify(this.state[stateProperty])) : null;
        }
    }

    addToBalance(val) {
        this.setState({
            balance: parseFloat((this.state.balance + parseFloat(val)).toFixed(2))
        })

    }

    removeFromBalance(val) {
        this.setState({
            balance: parseFloat((this.state.balance - parseFloat(val)).toFixed(2))
    })

}

addToAssets(queryArray) {
    this.setState({
        latestQuery: queryArray,
        assets: [...this.state.assets, queryArray]
    })
}

buyStock(stock) {
    const filterStock = this.state.myStock.filter(stockAsset => stockAsset.code === stock.code);
    if (filterStock.length === 0) {
        this.setState({
            myStock: [...this.state.myStock, stock]
        });
    } else {
        filterStock[0].amount = parseInt(filterStock[0].amount) + parseInt(stock.amount);
        this.setState({
            myStock: [...this.state.myStock]
        });
    }

    this.removeFromBalance(stock.cost)
}

sellStock(stockIndex, sellAmount) {
    const pSellAmount = parseInt(sellAmount);
    const stockAsset = this.state.myStock[stockIndex];
    if (pSellAmount < 1 || pSellAmount > stockAsset.amount) {
        throw "Invalid sell amount!"
    } else {
        let price;
        const fetchIndex = () => {
            const fetchString = api_url + "prices?identifier=" + stockAsset.code + "&start_date=" + getWeekBeforeTodayDate(7);

            const promise = fetch(fetchString, {
                method: "GET",
                headers: {
                    'Authorization': auth
                }
            }).then(response => response.json())
                .then(response => response.data)
                .then((queryResult) =>
                    price = queryResult[0].close)
                .catch(err => {
                    console.log('Unable to fetch the this resource. Err -> ', err);
                });
            return promise;
        }

        fetchIndex().then(() => {
            const cost = price * pSellAmount;
            stockAsset.amount = stockAsset.amount - pSellAmount;
            this.setState({
                myStock: [...this.state.myStock]
            })
            this.addToBalance(cost);
        });

    }
}

render() {
    return (
        <div className="container">
            <div className="page-title">
                Stock Asset Manager
                    {!this.state.isMyWallet ? <Search addToAssets={this.addToAssets} /> : null}
            </div>
            <Menu />
            {this.state.isMyWallet ? (
                <MyWallet balance={this.state.balance}
                    addToBalance={this.addToBalance}
                    removeFromBalance={this.removeFromBalance}
                    sellStock={this.sellStock}
                    assets={this.state.myStock} />
            ) : (
                    <Home balance={this.state.balance} latestQuery={this.state.latestQuery} buyStock={this.buyStock} />
                )}
            <div className="footer">
                <p>Copyright © Ventress 2018. All rights reserved.</p>
            </div>
        </div>
    );
}
}

App.propTypes = {
    location: PropTypes.object.isRequired
}