import React from "react";
import {StockAsset} from "./StockAsset";
import PropTypes from 'prop-types';

const Balance = (props) => {
    return (
        <div>Balance: {props.balance} $</div>
    )
}

const Operation = (props) => {
    return (
        <div className="operation">
            {props.operationMessage}
        </div>
    )
}
class Home extends React.Component {

    constructor() {
        super();
        this.defaultState = {
            operationShow: false,
            operationMessage: ""
        }
        this.state = this.defaultState;

        this.updateOperationMessage = this.updateOperationMessage.bind(this);
        this.updateOperationStatus = this.updateOperationStatus.bind(this);
    }

    updateOperationMessage(message) {
        this.setState({
            operationMessage: message
        });
    }
    updateOperationStatus(status) {
        this.setState({
            operationShow: status
        });
    }

    render() {
        return (
            <div className="my-wallet">
                <div className="balance-operation">
                    <Balance balance={this.props.balance} />
                    {this.state.operationShow ? <Operation operationMessage={this.state.operationMessage} /> : null}
                </div>
                <div className="stock-list">
                    <div className="stock-metadata">
                        Query results:
                </div>
                    {this.props.latestQuery ? this.props.latestQuery.map((asset, index) => {
                        return (
                            <StockAsset
                                key={index}
                                code={asset.ticker}
                                name={asset.name}
                                balance={this.props.balance}
                                buyStock={this.props.buyStock}
                                updateOperationStatus={this.updateOperationStatus}
                                updateOperationMessage={this.updateOperationMessage}
                            />
                        )
                    }) : <span>No data</span>}
                </div>
            </div>
        );
    }
}

Balance.propTypes = {
    balance: PropTypes.number.isRequired
}

Operation.propTypes = {
    operationMessage: PropTypes.number.isRequired
}

Home.propTypes = {
    balance: PropTypes.number.isRequired,
    buyStock: PropTypes.func.isRequired,
    latestQuery: PropTypes.array
}

export { Home, Balance };