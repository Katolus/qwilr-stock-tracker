import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import App from "./components/App";
import { Whoops } from "./components/Whoops";


export default () => (
  <BrowserRouter>
    <Switch>
      {/* <Route exact path="/" render={(props) =>  <App {...props}/>} />
      <Route exact path="/wallet" render={(props) =>  <App {...props}/>}  /> */}
      <Route exact path="/" component={App} />
      <Route exact path="/wallet" component={App} />
      <Route component={Whoops} />
    </Switch>
  </BrowserRouter>
);